<!DOCTYPE html>
<?php
session_start();
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Story</title>
</head>
<body id="regbody">
<h1 id="title2"><strong> Link your article! </strong></h1>
<div id="storystuff">
    <form method="POST">
        <p>
            <label for="title">Title:</label>
            <input type="text" name="title" id="title" class="title"/><br>
            <label for="link">Link URL:</label>
            <input type="text" name="link" id="link" class="link"/><br>
        <p>Description:</p>
        <textarea name="description" id="description" class="description"></textarea><br>
        <label for="article">Ready to publish your masterpiece?</label>
        <input type="submit" name="submit" value="Publish" id="article"/><br>
        <input type="submit" name="submit" value="Home"/><br>
        <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>"/>
        <input type="submit" name="submit" value="Log Out"/><br>
        </p>
    </form>
</div>

<div id="php"><?php
    /**
     * Created by PhpStorm.
     * User: Ben
     * Date: 9/28/2016
     * Time: 9:08 PM
     */
    require 'database.php';
    $user = $_SESSION['user'];
    $link = "";
    $title = "";

        if ($user == 'guest') {
            header("Location: home.php");
        }
        if (isset($_POST['description']) && isset($_POST['submit'])) {
            $title = $_POST['title'];
            $link = $_POST['link'];
            $description = $_POST['description'];
        }
        if (!strstr($link, "http")) {
            $link = "http://" . $link;
        }

        $stmt = NULL;
        if (isset($_POST['submit'])) {
            if ($_SESSION['token'] != $_POST['token']) {
                echo("Request forgery detected");
            } else {
            if ($_POST['submit'] == "Home") {
                header("Location: home.php");
            } elseif (($_POST['submit'] == "Log Out")) {
                array();
                session_unset();  //clear the session before logout
                session_destroy();
                header("Location: Login.php");
            } elseif (($_POST['submit'] == "Publish")) {

                $stmt = $mysqli->prepare("SELECT id from users where user_name = ?");
                $stmt->bind_param('s', $user);
                $stmt->execute();

                $stmt->bind_result($userID);
                $stmt->fetch();
                $stmt->close();


                $stmt1 = $mysqli->prepare("INSERT into news(title, description, link, author, author_id) VALUES(?,?,?,?,?)");
                if (!$stmt1) {
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt1->bind_param('sssss', $title, $description, $link, $user, $userID);
                $stmt1->execute();
                $stmt1->close();
                header("Location: home.php");
            }


        }
    }

    ?></div>



