<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>HomePage</title>
</head>
<body>
<h1 id="title2"><strong> Welcome! </strong></h1>
<form method="POST">
    <?php
    session_start();
    if (!isset($_SESSION['user'])) {     //for safety, so you cant change link on website and see files
        header("Location: Login.php");
    }
    require 'database.php';
    $user = $_SESSION['user'];
    echo("Logged in as: ");
    echo($user);
    ?>

    <input type="submit" name="submit" value="Log Out"/><br><br>
    <label for="article"> Have a story? Get typing! </label>
    <input type="submit" name="submit" value="Go!" id="article">
    <input type="submit" name="submit" value="Profile">
    <br><br>


</form>
<?php

echo("<br>");
$stmt = $mysqli->prepare("SELECT MAX(likes) from news");
if (!empty($stmt)) {
    //   $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->bind_result($cnt);
    $stmt->fetch();
    $stmt->close();
}

for ($i = $cnt; $i >= 0; $i--) {
    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM news WHERE likes=?");
    if ($stmt != false) {
        $stmt->bind_param('i', $i);
        $stmt->execute();
        $stmt->bind_result($rows);
        $stmt->fetch();
        $stmt->close();
        if ($rows > 1) {
            for ($x = 0; $x <= $cnt; $x++) {
                $stmt = $mysqli->prepare("SELECT title, link, description, author, id, likes FROM news WHERE likes=? AND id=? ");
                $stmt->bind_param('ii', $i, $x);
                $stmt->execute();
                $stmt->bind_result($title, $link, $description, $author, $id, $likes);
                $stmt->fetch();
                $stmt->close();
                if ( $likes >= 0 && $author != NULL) {
                    echo("<div class='homeform'><form method='POST'>");
                    echo("Author: $author");
                    echo("<br>");
                    echo("<div class='link'><a href=$link>$title</a></div>");
                    echo("Description: $description <br> ");
                    echo("$likes Likes");
                    echo("<div class='homebuttons'><input type='submit' name = 'submit' value = 'delete'>");
                    echo("<input type='submit' name = 'submit' value = 'edit' ></div>");
                    echo("<input type='submit' name = 'submit' value = 'Like' >");
                    echo("<input type='hidden' name = author value = $author >");
                    echo("<input type='hidden' name = id value = $id>");
                    echo("<input type='hidden' name = likes value = $likes>");
                    echo("<div class='commentlink'><a href='comments.php?varname=$id' > comments</a></div>");
                    //echo("<input type='submit' name='submit' value='comment' >");
                    echo("</form>");
                    echo("<br><br>");
                    //  }

                }
            }
        }
        elseif
        ($rows > 0) {
            $stmt = $mysqli->prepare("SELECT title, link, description, author, id, likes FROM news WHERE likes=? ");
            $stmt->bind_param('i', $i);
            $stmt->execute();
            $stmt->bind_result($title, $link, $description, $author, $id, $likes);
            $stmt->fetch();
            $stmt->close();

            echo("<div class='homeform'><form method='POST'>");
            echo("Author: $author");
            echo("<br>");
            echo("<div class='link'><a href=$link>$title</a></div>");
            echo("Description: $description <br> ");
            if ($likes == NULL) {
                echo("0 Likes");
            } else {
                echo("$likes Likes");
            }
            echo("<div class='homebuttons'><input type='submit' name = 'submit' value = 'delete'>");
            echo("<input type='submit' name = 'submit' value = 'edit' ></div>");
            echo("<input type='submit' name = 'submit' value = 'Like' >");
            echo("<input type='hidden' name = author value = $author >");
            echo("<input type='hidden' name = id value = $id>");
            echo("<input type='hidden' name = likes value = $likes>");
            echo("<div class='commentlink'><a href='comments.php?varname=$id' > comments</a></div>");
            //echo("<input type='submit' name='submit' value='comment' >");
            echo("</form></div>");
            echo("<br><br>");

        }
    }
}


if (isset($_POST['submit'])) {
    if ($_POST['submit'] == "Log Out") {
        array();
        session_unset();  //clear the session before logout
        session_destroy();
        header("Location: Login.php");
    } elseif ($_POST['submit'] == "Go!") {
        if ($user == "guest") {
            echo("Only registered users can post stories");
        } else {
            header("Location: story.php");
        }
    } elseif ($_POST['submit'] == "Profile") {
        header("Location: profile.php");
    } elseif ($_POST['submit'] == 'comment') {
        header("Location: comments.php");
    } elseif ($_POST['submit'] == "delete") {
        if ($user == "guest") {
            echo("Only registered users can delete stories");
        } else {
            $artID = $_REQUEST['id'];
            $checkAuthor = $_REQUEST['author'];
            if ($checkAuthor == $user) {
                $stmt = $mysqli->query("DELETE from news where id = $artID");
                header("Location: home.php");
            } else {
                echo("You can only delete your own stories.");
            }
        }
    } elseif ($_POST['submit'] == "edit") {
        if ($user == "guest") {
            echo("Only registered users can edit stories");
        } else {
            $artID = $_REQUEST['id'];
            $checkAuthor = $_REQUEST['author'];
            if ($checkAuthor == $user) {
                header("Location: editStory.php?varname=$artID");
            } else {
                echo("You can only edit your own stories.");
            }
        }
    } elseif ($_POST['submit'] == "Like") {
        if ($user == "guest") {
            echo("Only registered users can edit stories");
        } else {
            $artID = $_REQUEST['id'];
            $numLikes = (float)($_POST['likes'] + 1);
            $stmt = $mysqli->query("UPDATE news SET likes = $numLikes WHERE id=$artID");
            header("Location: home.php");
        }
    }
}
?>

</body>