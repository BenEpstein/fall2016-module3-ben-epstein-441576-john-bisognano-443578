<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit comment</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php
require 'database.php';
session_start();
if (!isset($_SESSION['user'])) {
    session_destroy();
    header("Location: Login.php");
    exit;
}
$user = $_SESSION['user'];
if ($user == 'guest') {
    header("Location: home.php");
}
$user = $_SESSION['user'];
$articleID = $_GET['varname'];


$stmt = $mysqli->prepare("SELECT id, title, description, link, author, author_id FROM news WHERE id=?");
$stmt->bind_param('s', $articleID);
$stmt->execute();
$stmt->bind_result($id, $title, $description, $link, $author, $author_id);
$stmt->fetch();
$stmt->close();
echo("<form method=\"POST\" action=''>");
echo("<textarea name='descriptionT' id='description' class='description'>$title</textarea> <br>");
echo("<textarea name='descriptionL' id='description' class='description'>$link</textarea> <br>");
echo("<textarea name='descriptionD' id='description' class='description'>$description</textarea> <br>");
echo("<input type='submit' name = 'submit' value = 'Update' >");
echo("</form>");

if (isset($_POST['submit']) && $_POST['submit'] == 'Update') {
    $title = $_POST['descriptionT'];
    $link = $_POST['descriptionL'];
    $description = $_POST['descriptionD'];
    if (!strstr($link, "http")) {
        $link = "http://" . $link;
    }
    $stmt = $mysqli->prepare("UPDATE news SET title=?, description=?, link=? WHERE id=?");
    $stmt->bind_param('ssss', $title, $description, $link, $articleID);
    $stmt->execute();
    $stmt->close();
    header("Location: home.php");
}


