<?php
require 'database.php';

if ($_POST['submit'] == 'Log In') {
    session_start();
    $_SESSION['token'] = substr(md5(rand()), 0, 10);
    $user = $mysqli->real_escape_string($_POST['user']);
    $stmt = $mysqli->prepare("SELECT COUNT(*), id, password FROM users WHERE user_name=?");
    var_dump($stmt);
// Bind the parameter
    $stmt->bind_param('s', $user);
    $stmt->execute();

// Bind the results
    $stmt->bind_result($cnt, $user_id, $pwd_hash);
    $stmt->fetch();

    $pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
    if ($cnt == 1 && crypt($pwd_guess, $pwd_hash) == $pwd_hash) {
        // Login succeeded!
        $_SESSION['user'] = $user;
        header("Location: home.php");
    } else {
        // Login failed; redirect back to the login screen
        header("Location: Login.php");
    }
} elseif ($_POST['submit'] == 'Register!') {
    header("Location: register.php");
} elseif ($_POST['submit'] == 'Guest Login') {
    session_start();
    $_SESSION['token'] = substr(md5(rand()), 0, 10);
    $_SESSION['user'] = "guest";
    header("Location: home.php");
}
