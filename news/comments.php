<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Story</title>
</head>
<body>
<h1 id="title2"><strong> Comments </strong></h1>
<div id="storystuff"><form method="POST" >
    <p>
        <textarea name="post" id="post" class="description">No comment</textarea><br>
        <label for="post">Share your opinion:</label>
        <input type="submit" name="submit" value="Comment"/><br>
        <input type='submit' name='submit' value='Log Out'>
        <input type='submit' name='submit' value='home'>
    </p>
</form></div>
<?php
require 'database.php';
$id = $_GET['varname'];

session_start();
$user = $_SESSION['user'];     //for safety, so you cant change link on website and see files


$user = $_SESSION['user'];
echo("Logged in as: ");
echo($user);
echo("<br><br>");
//now I need to get the link, so use $id to get news table (author_id)
$stmt = $mysqli->prepare("SELECT title, description, link FROM news WHERE id=?");
$stmt->bind_param('s', $id);
$stmt->execute();
$stmt->bind_result($title, $description, $link);
$stmt->fetch();
$stmt->close();

echo("<a href = $link> $title</a><br>");
echo("$description<br>");
echo("$link<br><br>");

$stmt = $mysqli->prepare("SELECT MAX(comment_id) FROM comments");
if (!empty($stmt)) {
    $stmt->execute();
    $stmt->bind_result($cnt);
    $stmt->fetch();
    $stmt->close();
}
for ($i = 1; $i <= $cnt; $i++) {
    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM comments WHERE article_id=?");
    if ($stmt != false) {
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($rows);
        $stmt->fetch();
        $stmt->close();
        if ($rows > 0) {
            $stmt = $mysqli->prepare("SELECT comment_id, commenter, comment FROM comments WHERE comment_id=? AND article_id=?");
            if (!empty($stmt)) {
                $stmt->bind_param('ss', $i, $id);
                $stmt->execute();
                $stmt->bind_result($comment_id, $commenter, $comment);
                $stmt->fetch();
                $stmt->close();


                if ($commenter != "") {
                    echo("Commenter: $commenter");
                    echo("<br>");
                    echo("$comment <br> ");
                    echo("<form method=\"POST\">");
                    echo("<input type='submit' name = 'submit' value = 'delete'>");
                    echo("<input type='submit' name = 'submit' value = 'edit' >");
                    echo("<input type='hidden' name = commenter value = $commenter >");
                    echo("<input type='hidden' name = commentID value = $comment_id >");
                    echo("</form>");
                }

            }
        }
    }
}
if (isset($_POST['submit'])) {
    if ($_POST['submit'] == 'Comment') {
        if ($user == "guest") {
            echo("Only registered users can post comments");
        } else {
            $value = $_POST['post'];
            $stmt = $mysqli->prepare("INSERT into comments(article_id, commenter, comment) values (?,?,?)");
            $stmt->bind_param('sss', $id, $user, $value);
            $stmt->execute();
            $stmt->close();
            header("Location: comments.php?varname=$id");

        }

    } elseif ($_POST['submit'] == "Log Out") {
        array();
        session_unset();  //clear the session before logout
        session_destroy();
        header("Location: Login.php");
    } elseif ($_POST['submit'] == "delete") {
        if ($user == "guest") {
            echo("Only registered users can delete comments");
        } else {
            $getID = $_REQUEST['commentID'];
            $commenter1 = $_REQUEST['commenter'];
            if ($commenter1 == $user) {
                $stmt = $mysqli->query("DELETE from comments where comment_id = $getID");
                header("Location: comments.php?varname=$id");
            } else {
                echo("You can only delete your own comments.");
            }
        }
    } elseif ($_POST['submit'] == "edit") {
        if ($user == "guest") {
            echo("Only registered users can edit comments");
        } else {
            $getID = $_POST['commentID'];
            $commenter1 = $_POST['commenter'];
            if ($commenter1 == $user) {
                header("Location: editComment.php?varname=$getID");
            } else {
                echo("You can only edit your own comments.");
            }
        }
    }
    elseif($_POST['submit'] == 'home'){
        header("Location: home.php");
    }
}
?>


