<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Edit comment</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php
require 'database.php';
session_start();
if (!isset($_SESSION['user'])) {
    session_destroy();
    header("Location: Login.php");
    exit;
}
$user = $_SESSION['user'];
if ($user == 'guest') {
    header("Location: home.php");
}
$user = $_SESSION['user'];
$comment_id = $_GET['varname'];


$stmt = $mysqli->prepare("SELECT article_id, commenter, comment  FROM comments WHERE comment_id=?");
$stmt->bind_param('s', $comment_id);
$stmt->execute();
$stmt->bind_result($article_id, $commenter, $comment);
$stmt->fetch();
$stmt->close();
echo("<form method=\"POST\" action=''>");
echo("<textarea name='description' id='description' class='description'> $comment </textarea> <br>");
echo("<input type='submit' name = 'submit' value = 'change' >");
//echo("<input type='hidden' name = commenter value = $commenter >");
//echo("<input type='hidden' name = commentID value = $articleID >");
echo("</form>");
if (isset($_POST['submit']) && $_POST['submit'] == 'change') {
    $editted = $_POST['description'];
    $stmt = $mysqli->prepare("UPDATE comments SET comment=? WHERE comment_id=?");
    $stmt->bind_param('ss', $editted, $comment_id);
    $stmt->execute();
    $stmt->close();
    header("Location: comments.php?varname=$article_id");
}


