<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>New User</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body id="regbody">

<h1 id="title2"> Thank you for registering with us! </h1>
<form method="post">
    <p id="forms">
        <label for="first">First Name:</label>
        <input type="text" name="first" id="first"/>
        <label for="last">Last Name:</label>
        <input type="text" name="last" id="last"/>
        <label for="userName">User Name:</label>
        <input type="text" name="userName" id="userName"/>
        <label for="password"> Password:</label>
        <input type='password' name="password" id='password'>
        <label for="email">Email address:</label>
        <input type="text" name="email" id="email"/>
    </p>
    <div id="regbutton"><input type="submit" name="action" value="Register!" id="submit"><br></div>
    <div id="logbutton"><input type="submit" name="action" value="Log In" id="submit"></div>


</form>

<div id="php"><?php
/**
 * Created by PhpStorm.
 * User: Ben
 * Date: 9/26/2016
 * Time: 12:29 PM
 */

require 'database.php';
if (isset($_POST['first']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['last']) && isset($_POST['userName'])) {
    if ($_POST['action'] == "Register!") {
        $first = $_POST['first'];
        $last = $_POST['last'];
        $userName = $_POST['userName'];
        $email = $_POST['email'];
        $pass = crypt($_POST['password']);

        if (($_POST['first'] == "") || $_POST['email'] == "" || ($_POST['password'] == "") || ($_POST['last'] == "") ||
            ($_POST['userName'] == "")
        ) {
            echo("Please fill out all forms");
        } else {
            $stmt = $mysqli->prepare("SELECT COUNT(*) FROM users WHERE user_name=?");

            $stmt->bind_param('s', $userName);
            $stmt->execute();

            $stmt->bind_result($cnt);
            $stmt->fetch();
            $stmt->close();

            if ($cnt == 0) {
                $stmt = $mysqli->prepare("insert into users (first_name, last_name, user_name, email, password) values (?, ?, ?, ?, ?)");
                if (!$stmt) {
                    var_dump($stmt);
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
                }
                $stmt->bind_param('sssss', $first, $last, $userName, $email, $pass);
                $stmt->execute();
                $stmt->close();
                echo("Success! Please log in now!");
            } else {
                echo("User Name already exists");
            }
        }
    }
}
if (isset($_POST['action'])) {
    if ($_POST['action'] == "Log In") {
        header("Location: Login.php");
    }
}

?></div>

</body>
</html>
