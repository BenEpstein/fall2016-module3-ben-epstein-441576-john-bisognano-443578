<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Profile</title>
</head>
<body>
<h1 id="title2"><strong> Profile </strong></h1>
<?php
session_start();
if (!isset($_SESSION['user'])) {     //for safety, so you cant change link on website and see files
    header("Location: Login.php");
}
require 'database.php';
$user = $_SESSION['user'];
echo("Logged in as: ");
echo($user);
echo("<form method='POST'>");
echo("<input type='submit' name = 'submit' value = 'Home' >");
echo("<input type='submit' name = 'submit' value = 'Log Out' >");
echo("</form>");

if (isset($_POST['submit'])) {
    if ($_POST['submit'] == "Log Out") {
        array();
        session_unset();  //clear the session before logout
        session_destroy();
        header("Location: Login.php");
    } elseif ($_POST['submit'] == "Home") {
        header("Location: home.php");
    }
}

echo("<h1> Stories</h1>");
$stmt = $mysqli->prepare("SELECT MAX(id) from news WHERE author=?");
if (!empty($stmt)) {
    //   $stmt->bind_param('s', $user);
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->bind_result($cnt);
    $stmt->fetch();
    $stmt->close();
}

for ($i = 0; $i <= $cnt; $i++) {
    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM news WHERE author=? AND id=?");
    if ($stmt != false) {
        $stmt->bind_param('si', $user, $i);
        $stmt->execute();
        $stmt->bind_result($rows);
        $stmt->fetch();
        $stmt->close();
        if ($rows > 0) {
            $stmt = $mysqli->prepare("SELECT title, link, description, id FROM news WHERE author=? AND id=?");
            $stmt->bind_param('si', $user, $i);
            $stmt->execute();
            $stmt->bind_result($title, $link, $description, $id);
            $stmt->fetch();
            $stmt->close();

            echo("<div id='profstoryform'><form method='POST'>");
            echo("<div id='proflink'><a href=$link>$title</a></div>");
            echo("Description: $description <br> ");
            echo("<div id='profstorybuttons'><input type='submit' name = 'submit' value = 'delete'>");
            echo("<input type='submit' name = 'submit' value = 'edit' ></div>");
            echo("<input type='hidden' name = author value = $user >");
            echo("<input type='hidden' name = id value = $id>");
            echo("<div id='profcommentlink'><a href='comments.php?varname=$id' > comments</a></div>");
            //echo("<input type='submit' name='submit' value='comment' >");
            echo("</form></div>");
            echo("<br><br>");

        }
    }
}


if (isset($_POST['submit'])) {
    if ($_POST['submit'] == "Log Out") {
        array();
        session_unset();  //clear the session before logout
        session_destroy();
        header("Location: Login.php");
    } elseif ($_POST['submit'] == "Go!") {
        if ($user == "guest") {
            echo("Only registered users can post stories");
        } else {
            header("Location: story.php");
        }
    } elseif ($_POST['submit'] == "delete") {
        if ($user == "guest") {
            echo("Only registered users can delete stories");
        } else {
            $artID = $_REQUEST['id'];
            $checkAuthor = $_REQUEST['author'];
            if ($checkAuthor == $user) {
                $stmt = $mysqli->query("DELETE from news where id = $artID");
                header("Location: home.php");
            } else {
                echo("You can only delete your own stories.");
            }
        }
    } elseif ($_POST['submit'] == "edit") {
        if ($user == "guest") {
            echo("Only registered users can edit stories");
        } else {
            $artID = $_REQUEST['id'];
            $checkAuthor = $_REQUEST['author'];
            if ($checkAuthor == $user) {
                header("Location: editStory.php?varname=$artID");
            } else {
                echo("You can only edit your own stories.");
            }
        }
    }
}


//now we're going to show all comments on the column to the left of stories and show which story they came from:

echo("<h1> Comments </h1>");
$stmt = $mysqli->prepare("SELECT MAX(comment_id) FROM comments WHERE commenter=?");
if (!empty($stmt)) {
    $stmt->bind_param('s', $user);
    $stmt->execute();
    $stmt->bind_result($cnt);
    $stmt->fetch();
    $stmt->close();
}
for ($i = 1; $i <= $cnt; $i++) {
    $stmt = $mysqli->prepare("SELECT COUNT(*) FROM comments WHERE commenter=?");
    if ($stmt != false) {
        $stmt->bind_param('i', $user);
        $stmt->execute();
        $stmt->bind_result($rows);
        $stmt->fetch();
        $stmt->close();
        if ($rows > 0) {
            $stmt = $mysqli->prepare("SELECT comment_id, article_id, comment FROM comments WHERE comment_id=? AND commenter=?");
            if (!empty($stmt)) {
                $stmt->bind_param('ss', $i, $user);
                $stmt->execute();
                $stmt->bind_result($comment_id, $article_id, $comment);
                $stmt->fetch();
                $stmt->close();
                $stmt = $mysqli->prepare("SELECT title from news WHERE id=?");
                if (!empty($stmt)) {
                    $stmt->bind_param('s', $article_id);
                    $stmt->execute();
                    $stmt->bind_result($storyTitle);
                    $stmt->fetch();
                    $stmt->close();
                }
                if ($comment != NULL) {
                    echo("Story: <a href = 'comments.php?varname=$article_id'>$storyTitle</a>");
                    echo("<br>");
                    echo("$comment <br> ");
                    echo("<form method=\"POST\">");
                    echo("<input type='submit' name = 'submit' value = 'delete comment'>");
                    echo("<input type='submit' name = 'submit' value = 'edit comment' >");
                    echo("<input type='hidden' name = commentID value = $comment_id >");
                    echo("</form>");
                }
            }
        }
    }
}
if (isset($_POST['submit'])) {
    if ($_POST['submit'] == "delete comment") {
        if ($user == "guest") {
            echo("Only registered users can delete comments");
        } else {
            $getID = $_REQUEST['commentID'];
            $stmt = $mysqli->query("DELETE from comments where comment_id = $getID");
            header("Location: profile.php");
        }
    } elseif ($_POST['submit'] == "edit comment") {
        if ($user == "guest") {
            echo("Only registered users can edit comments");
        } else {
            $getID = $_POST['commentID'];
            header("Location: editComment.php?varname=$getID");
        }
    }
}

?>