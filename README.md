# Simple News Web Site

You should use PHP unless you get permission to use another web technology from the instructor. You may also use phpMyAdmin to help set up databases.
You may find this wiki article helpful: PHP and MySQL

**Requirements**

1. Users can register for accounts and then log in to the website.
2. Accounts should have both a username and a secure password. NEVER store plaintext passwords in a database!
For more information on password security, refer to the Web Application Security guide.
3. Registered users can submit story commentary.
4. A link can be associated with each story, and they should be stored in a separate database field from the story
5. Registered users can comment on any story.
6. Unregistered users can only view stories and comments.
7. Registered users can edit and delete their stories and comments.
8. All data must be kept in a MySQL database (user information, stories, comments, and links).

###Creative Portion
**Web Security and Validation**

No application is finished until much thought is put into web security and best practice. Throughout this course, we heavily emphasize the dogma of responsible coding.
Read this week's Web Application Security guide: Web Application Security, Part 2. In particular, your project needs to:

* Your application needs to be secure from SQL injection attacks. If you are using prepared queries, you should already be safe on this front.
* Your passwords need to be securely salted to prevent rainbow table attacks.
* You should pass tokens in forms to prevent CSRF attacks.
* You should check all preconditions on the server side to prevent Abuse of Functionality attacks.
* You shouldn't forget the practices you learned last week:
* Your page should validate with no errors through the W3C validator.
* Filter Input and Escape Output, but not the other way around.